package com.solution.health_manager.usermanagementservice.persistence.repository;

import com.solution.health_manager.usermanagementservice.persistence.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByName(String username);
}
